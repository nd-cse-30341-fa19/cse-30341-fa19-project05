/* page.h: MemHashed Page Structure */

#ifndef PAGE_H
#define PAGE_H

#include "memhashed/thread.h"
#include "memhashed/types.h"

/* Entry Structure */

typedef struct {
    uint64_t    key;	        /* Entry key */
    int64_t     value;          /* Entry value */

    bool        valid;          /* Whether or not Entry is valid */
    size_t      used;           /* How recently this Entry was used (higher is more recent) */
} Entry;

/* Page Structure */

typedef struct {
    Policy      policy;         /* Eviction policy */
    size_t      nentries;       /* Number of entries */

    Entry      *entries;        /* Array of entries */
    
    size_t      used;	        /* Used counter (CLOCK) */
    size_t      clock_hand;     /* Clock hand (CLOCK) */

    Mutex       lock;           /* Lock */
} Page;

/* Page Functions */

Page *      page_create(size_t nentries, Policy policy);
void        page_delete(Page *page);

Entry       page_get(Page *page, const uint64_t key, size_t offset);
void        page_put(Page *page, const uint64_t key, const int64_t value, size_t offset);

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
