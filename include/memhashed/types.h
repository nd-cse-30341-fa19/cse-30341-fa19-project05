/* types.h: MemHashed types */

#ifndef TYPES_H
#define TYPES_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/* Handler Function Type */

typedef int64_t (*Handler)(const uint64_t);

/* Eviction Policy Enumeration */

typedef enum {
    RANDOM,
    CLOCK,
} Policy;

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
