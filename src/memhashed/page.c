/* page.c: MemHashed Page structure */

#include "memhashed/page.h"

/* Prototypes */

size_t  page_evict_random(Page *page, size_t offset);
size_t  page_evict_clock(Page *page, size_t offset);

/* Functions */

/**
 * Create Page Structure.
 * @param   nentries    Number of entries in page.
 * @param   policy      Which eviction policy to use.
 * @return  Newly allocated Page Structure with allocated entries.
 */
Page *	page_create(size_t nentries, Policy policy) {
    // TODO: Allocate Page Structure and internal fields.
    return NULL;
}

/**
 * Delete Page Structure (including internal entries).
 * @param   page        Pointer to Page Structure.
 */
void    page_delete(Page *page) {
    // TODO: Deallocate Page Structure and internal fields.
}

/**
 * Searches Page for Entry with specified key, beginning at specified offset.
 *
 *  This function performs a linear probe of the entries array beginning with
 *  the specified offset until a matching key is found or all entries have been
 *  probed.
 *
 * @param   page        Pointer to Page Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @param   offset      Initial offset to begin probe.
 * @return  Entry with corresponding key or NotFound if not found.
 */
Entry   page_get(Page *page, const uint64_t key, size_t offset) {
    Entry entry = {.key = ~key};
    return entry;
}

/**
 * Insert or update Entry with specified key with the new value.
 *
 *  This function begins a linear probe for the Entry with the corresponding
 *  key:
 *
 *      1. If it is found, then the Entry is updated.
 *
 *      2. If it is not found, then the first invalid Entry is used to insert a
 *      new key and value.
 *
 *      3. If it is not found and there are no invalid Entries, then one is
 *      evicted based on the previously specified Policy and then used
 *      to insert a new key and value.
 *
 * @param   page        Pointer to Page Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @parm    value       Value to update or replace old value with.
 * @param   offset      Initial offset to begin probe.
 */
void    page_put(Page *page, const uint64_t key, const int64_t value, size_t offset) {
}

/**
 * Select Entry to evict based on Random strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_random(Page *page, size_t offset) {
    return offset;
}

/**
 * Select Entry to evict based on Clock strategy.
 * @param   page        Pointer to Page Structure.
 * @param   offset      Initial offset to begin probe.
 * @return  Offset of Entry to evict.
 */
size_t  page_evict_clock(Page *page, size_t offset) {
    return offset;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
