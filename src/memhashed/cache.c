/* cache.c: MemHashed Cache structure */

#include "memhashed/cache.h"

/* Cache Functions */

/**
 * Create Cache Structure.
 * @param   addrlen     Length of virtual address.
 * @param   page_size   Number of entries per page.
 * @param   policy      Which eviction policy to use.
 * @param   handler     Handler function to call on cache misses.
 * @return  Newly allocated Cache Structure with allocated pages.
 */
Cache *	cache_create(size_t addrlen, size_t page_size, Policy policy, Handler handler) {
    // TODO: Allocate Cache Structure and configure internal fields.
    return NULL;
}

/**
 * Delete Cache Structure (including internal pages).
 * @param   cache       Pointer to Cache Structure.
 */
void    cache_delete(Cache *cache) {
    // TODO: Deallocate Cache Structure and internal fields.
}

/**
 * Retrieves value for specified for key:
 *
 *  1. Generate the virtual address.
 *
 *  2. Compute VPN and offset of virtual ddress using cache masks and shifts.
 *
 *  3. Use VPN and offset to lookup appropriate page:
 *
 *      a. If entry is found, then return value.
 *
 *      b. Otherwise, use the handler to generate the value for the
 *      corresponding key.
 *
 * @param   cache       Pointer to Cache Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @return  value for the correspending key.
 */
int64_t	cache_get(Cache *cache, const uint64_t key) {
    return 0;
}

/**
 * Insert or update value for specified key:
 *
 *  1. Generate the virtual address.
 *
 *  2. Compute VPN and offset of virtual ddress using cache masks and shifts.
 *
 *  3. Use VPN and offset to insert or update value into appropriate page.
 *
 * @param   cache       Pointer to Cache Structure.
 * @param   key         Key used to identify corresponding Entry.
 * @param   value       Value to update or replace old value with.
 */
void	cache_put(Cache *cache, const uint64_t key, const int64_t value) {
}

/**
 * Display cache hits and misses to specified streams.
 * @param   cache       Pointer to Cache Structure.
 * @param   stream      File stream to write to.
 */
void	cache_stats(Cache *cache, FILE *stream) {
    mutex_lock(&cache->lock);
    fprintf(stream, "Hits   = %lu\n", cache->hits);
    fprintf(stream, "Misses = %lu\n", cache->misses);
    mutex_unlock(&cache->lock);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
