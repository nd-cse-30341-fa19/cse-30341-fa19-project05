/* unit_page.c: Unit tests for Page Structure */

#include "memhashed/page.h"

#include <assert.h>
#include <limits.h>
#include <stdio.h>

/* Functions */

int test_00_page_create() {
    const size_t NENTRIES = 32;
    Page *page = page_create(NENTRIES, RANDOM);
    assert(page);
    assert(page->nentries == NENTRIES);
    assert(page->policy   == RANDOM);
    assert(page->entries  != NULL);
    page_delete(page);
    return EXIT_SUCCESS;
}

int test_01_page_get() {
    const size_t NENTRIES = 4;
    Page *page = page_create(NENTRIES, RANDOM);
    assert(page);
    assert(page->nentries == NENTRIES);
    assert(page->policy   == RANDOM);
    assert(page->entries  != NULL);

    for (int64_t p = 0; p < NENTRIES; p++) {
        page->entries[p].key   = p;
        page->entries[p].value = (int64_t)p;
        page->entries[p].valid = true;
        page->entries[p].used  = p + 1;
    }

    for (int64_t e = 0; e < NENTRIES; e++) {
        Entry entry = page_get(page, e, e);
        assert(entry.key   == page->entries[e].key);
        assert(entry.value == page->entries[e].value);
    }

    for (int64_t e = NENTRIES; e < 2*NENTRIES; e++) {
        Entry entry = page_get(page, e, e);
        assert(entry.key != e);
    }

    for (int64_t e = 0; e < NENTRIES; e++) {
        Entry entry = page_get(page, e, NENTRIES);
        assert(entry.key != page->entries[e].key);
    }

    page_delete(page);
    return EXIT_SUCCESS;
}

void test_XX_page_insert(Page *page, const size_t nentries) {
    // INSERT: Populate entries
    for (int64_t e = 0; e < nentries; e++) {
        page_put(page, e, (int64_t)e, e);
    }

    // Check that entries placed properly
    for (int64_t e = 0; e < nentries; e++) {
        Entry entry = page_get(page, e, e);
        assert(entry.key   == page->entries[e].key);
        assert(entry.key   == e);
        assert(entry.value == e);
        assert(entry.valid == true);
        assert(entry.used  == nentries + e + 1);
    }
}

void test_XX_page_update(Page *page, const size_t nentries) {
    // UPDATE: Overwrite entries
    for (int64_t e = 0; e < nentries; e++) {
        page_put(page, e, (int64_t)(nentries + e), e);
    }

    // Check that entries were overwritten (backwards)
    for (int64_t e = 0; e < nentries; e++) {
        size_t key   = nentries - e - 1;
        Entry  entry = page_get(page, key, key);
        assert(entry.key   == page->entries[key].key);
        assert(entry.key   == key);
        assert(entry.value == nentries + key);
        assert(entry.valid == true);
        assert(entry.used  == 3*nentries + e + 1);
    }
}

int test_02_page_put_random() {
    const size_t NENTRIES = 4;
    Page *page = page_create(NENTRIES, RANDOM);
    assert(page);
    assert(page->nentries == NENTRIES);
    assert(page->policy   == RANDOM);
    assert(page->entries  != NULL);

    test_XX_page_insert(page, NENTRIES);
    test_XX_page_update(page, NENTRIES);

    // REPLACE: Replace current entries
    for (int64_t e = 0; e < NENTRIES; e++) {
        page_put(page, NENTRIES + e, (int64_t)(NENTRIES + e), e);
    }

    // Check that entries were replaced
    for (int64_t e = 0; e < NENTRIES; e++) {
        size_t key   = NENTRIES + e;
        Entry  entry = page_get(page, key, e);
        if (entry.key == key) {
            assert(entry.value == key);
            assert(entry.valid == true);
        }
    }

    page_delete(page);
    return EXIT_SUCCESS;
}

int test_03_page_put_clock() {
    const size_t NENTRIES = 4;
    Page *page = page_create(NENTRIES, CLOCK);
    assert(page);
    assert(page->nentries == NENTRIES);
    assert(page->policy   == CLOCK);
    assert(page->entries  != NULL);

    test_XX_page_insert(page, NENTRIES);
    test_XX_page_update(page, NENTRIES);

    // REPLACE: Replace current entries
    for (int64_t e = 0; e < NENTRIES; e++) {
        page_put(page, NENTRIES + e, (int64_t)(NENTRIES + e), e);
    }

    // Check that entries were replaced
    for (int64_t e = 0; e < NENTRIES; e++) {
        size_t key   = NENTRIES + e;
        Entry  entry = page_get(page, key, e);
        assert(entry.key   == page->entries[e].key);
        assert(entry.key   == key);
        assert(entry.value == key);
        assert(entry.valid == true);
        assert(entry.used  == 5*NENTRIES + e + 1);
    }

    page_delete(page);
    return EXIT_SUCCESS;
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s NUMBER\n\n", argv[0]);
        fprintf(stderr, "Where NUMBER is right of the following:\n");
        fprintf(stderr, "    0. Test page_create\n");
        fprintf(stderr, "    1. Test page_get\n");
        fprintf(stderr, "    2. Test page_put_random\n");
        fprintf(stderr, "    3. Test page_put_clock\n");
        return EXIT_FAILURE;
    }

    int number = atoi(argv[1]);
    int status = EXIT_FAILURE;

    switch (number) {
        case 0:  status = test_00_page_create(); break;
        case 1:  status = test_01_page_get(); break;
        case 2:  status = test_02_page_put_random(); break;
        case 3:  status = test_03_page_put_clock(); break;
        default: fprintf(stderr, "Unknown NUMBER: %d\n", number); break;
    }

    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
