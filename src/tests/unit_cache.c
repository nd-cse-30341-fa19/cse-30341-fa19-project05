/* unit_cache.c: Unit tests for Cache Structure */

#include "memhashed/cache.h"

#include <assert.h>
#include <limits.h>

/* Functions */

int64_t echo_handler(const uint64_t key) {
    return (int64_t)key;
}

int test_00_cache_create() {
    Cache *cache = cache_create(0, 0, RANDOM, echo_handler);
    assert(cache);
    assert(cache->addrlen     == 0);
    assert(cache->page_size   == 0);
    assert(cache->policy      == RANDOM);
    assert(cache->handler     == echo_handler);
    assert(cache->naddresses  == 0);
    assert(cache->npages      == 0);
    assert(cache->vpn_shift   == 0);
    assert(cache->vpn_mask    == 0);
    assert(cache->offset_mask == 0);
    assert(cache->pages       == NULL);
    cache_delete(cache);

    cache = cache_create(8, 8, CLOCK, echo_handler);
    assert(cache);
    assert(cache->addrlen     == 8);
    assert(cache->page_size   == 8);
    assert(cache->policy      == CLOCK);
    assert(cache->handler     == echo_handler);
    assert(cache->naddresses  == 256);
    assert(cache->npages      == 32);
    assert(cache->vpn_shift   == 3);
    assert(cache->vpn_mask    == 0xf8);
    assert(cache->offset_mask == 0x07);
    assert(cache->pages       != NULL);
    cache_delete(cache);

    cache = cache_create(10, 4, RANDOM, echo_handler);
    assert(cache);
    assert(cache->addrlen     == 10);
    assert(cache->page_size   == 4);
    assert(cache->policy      == RANDOM);
    assert(cache->handler     == echo_handler);
    assert(cache->naddresses  == 1024);
    assert(cache->npages      == 256);
    assert(cache->vpn_shift   == 2);
    assert(cache->vpn_mask    == 0x3fc);
    assert(cache->offset_mask == 0x003);
    assert(cache->pages       != NULL);
    cache_delete(cache);
    return EXIT_SUCCESS;
}

int test_01_cache_get() {
    Cache *cache = cache_create(8, 8, RANDOM, echo_handler);
    assert(cache);

    for (uint64_t key = 0; key < 8; key++) {
        size_t address = key % 256;
        size_t vpn     = (address & 0xf8) >> 3;
        size_t offset  = (address & 0x07);

        cache->pages[vpn]->entries[offset].key   = key;
        cache->pages[vpn]->entries[offset].value = 2*key;
        cache->pages[vpn]->entries[offset].valid = true;
    }
    
    for (uint64_t key = 0; key < 8; key++) {
        assert(cache_get(cache, key) == 2*key);
    }
    
    for (uint64_t key = 8; key < 16; key++) {
        assert(cache_get(cache, key) == key);
    }

    cache_delete(cache);
    return EXIT_SUCCESS;
}

int test_02_cache_put() {
    Cache *cache = cache_create(8, 8, RANDOM, echo_handler);
    assert(cache);

    for (uint64_t key = 0; key < 8; key++) {
        cache_put(cache, key, 2*key);
    }
    
    for (uint64_t key = 0; key < 8; key++) {
        assert(cache_get(cache, key) == 2*key);
    }
    
    for (uint64_t key = 8; key < 16; key++) {
        assert(cache_get(cache, key) == key);
    }

    cache_delete(cache);
    return EXIT_SUCCESS;
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s NUMBER\n\n", argv[0]);
        fprintf(stderr, "Where NUMBER is right of the following:\n");
        fprintf(stderr, "    0. Test cache_create\n");
        fprintf(stderr, "    1. Test cache_get\n");
        fprintf(stderr, "    2. Test cache_puts\n");
        return EXIT_FAILURE;
    }

    int number = atoi(argv[1]);
    int status = EXIT_FAILURE;

    switch (number) {
        case 0:  status = test_00_cache_create(); break;
        case 1:  status = test_01_cache_get(); break;
        case 2:  status = test_02_cache_put(); break;
        default: fprintf(stderr, "Unknown NUMBER: %d\n", number); break;
    }

    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
