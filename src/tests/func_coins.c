/* func_coins.c: compute coins */

#include "memhashed/cache.h"
#include "memhashed/thread.h"

#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

/* Globals */

Cache *Coins = NULL;

/* Thread */

void *	coins_thread(void *arg) {
    // TODO: Compute number of coins need for amounts 0-99.
    //	printf("%2lu. Coins(%2lu) = %lu\n", Thread Id, Amount, Coins(Amount))

    return NULL;
}

/* Handler */

#define MIN(x, y)   ((x) < (y) ? (x) : (y))

int64_t	coins_handler(const uint64_t key) {
    // TODO: Implement Coins solution using dynamic programming
    return 0;
}

/* Main execution */

int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage: %s AddressLength PageSize EvictionPolicy Threads\n", argv[0]);
    	return EXIT_FAILURE;
    }

    // TODO: Parse command line arguments

    // TODO: Create Coins Cache

    // TODO: Create Worker threads

    // TODO: Join Worker threads

    // TODO: Output Coins Cache statistics

    // TODO: Delete Coins Cache
    return EXIT_SUCCESS;
}
