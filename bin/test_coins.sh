#!/bin/bash

coins-random-output() {
    cat <<EOF
 0. Coins( 0) = 0
 0. Coins( 1) = 1
 0. Coins( 2) = 2
 0. Coins( 3) = 3
 0. Coins( 4) = 4
 0. Coins( 5) = 1
 0. Coins( 6) = 2
 0. Coins( 7) = 3
 0. Coins( 8) = 4
 0. Coins( 9) = 5
 0. Coins(10) = 1
 0. Coins(11) = 2
 0. Coins(12) = 3
 0. Coins(13) = 4
 0. Coins(14) = 5
 0. Coins(15) = 2
 0. Coins(16) = 3
 0. Coins(17) = 4
 0. Coins(18) = 5
 0. Coins(19) = 6
 0. Coins(20) = 2
 0. Coins(21) = 3
 0. Coins(22) = 4
 0. Coins(23) = 5
 0. Coins(24) = 6
 0. Coins(25) = 1
 0. Coins(26) = 2
 0. Coins(27) = 3
 0. Coins(28) = 4
 0. Coins(29) = 5
 0. Coins(30) = 2
 0. Coins(31) = 3
 0. Coins(32) = 4
 0. Coins(33) = 5
 0. Coins(34) = 6
 0. Coins(35) = 2
 0. Coins(36) = 3
 0. Coins(37) = 4
 0. Coins(38) = 5
 0. Coins(39) = 6
 0. Coins(40) = 3
 0. Coins(41) = 4
 0. Coins(42) = 5
 0. Coins(43) = 6
 0. Coins(44) = 7
 0. Coins(45) = 3
 0. Coins(46) = 4
 0. Coins(47) = 5
 0. Coins(48) = 6
 0. Coins(49) = 7
 0. Coins(50) = 2
 0. Coins(51) = 3
 0. Coins(52) = 4
 0. Coins(53) = 5
 0. Coins(54) = 6
 0. Coins(55) = 3
 0. Coins(56) = 4
 0. Coins(57) = 5
 0. Coins(58) = 6
 0. Coins(59) = 7
 0. Coins(60) = 3
 0. Coins(61) = 4
 0. Coins(62) = 5
 0. Coins(63) = 6
 0. Coins(64) = 7
 0. Coins(65) = 4
 0. Coins(66) = 5
 0. Coins(67) = 6
 0. Coins(68) = 7
 0. Coins(69) = 8
 0. Coins(70) = 4
 0. Coins(71) = 5
 0. Coins(72) = 6
 0. Coins(73) = 7
 0. Coins(74) = 8
 0. Coins(75) = 3
 0. Coins(76) = 4
 0. Coins(77) = 5
 0. Coins(78) = 6
 0. Coins(79) = 7
 0. Coins(80) = 4
 0. Coins(81) = 5
 0. Coins(82) = 6
 0. Coins(83) = 7
 0. Coins(84) = 8
 0. Coins(85) = 4
 0. Coins(86) = 5
 0. Coins(87) = 6
 0. Coins(88) = 7
 0. Coins(89) = 8
 0. Coins(90) = 5
 0. Coins(91) = 6
 0. Coins(92) = 7
 0. Coins(93) = 8
 0. Coins(94) = 9
 0. Coins(95) = 5
 0. Coins(96) = 6
 0. Coins(97) = 7
 0. Coins(98) = 8
 0. Coins(99) = 9
Hits   = 2280
Misses = 186
EOF
}

coins-clock-output() {
    cat <<EOF
 0. Coins( 0) = 0
 0. Coins( 1) = 1
 0. Coins( 2) = 2
 0. Coins( 3) = 3
 0. Coins( 4) = 4
 0. Coins( 5) = 1
 0. Coins( 6) = 2
 0. Coins( 7) = 3
 0. Coins( 8) = 4
 0. Coins( 9) = 5
 0. Coins(10) = 1
 0. Coins(11) = 2
 0. Coins(12) = 3
 0. Coins(13) = 4
 0. Coins(14) = 5
 0. Coins(15) = 2
 0. Coins(16) = 3
 0. Coins(17) = 4
 0. Coins(18) = 5
 0. Coins(19) = 6
 0. Coins(20) = 2
 0. Coins(21) = 3
 0. Coins(22) = 4
 0. Coins(23) = 5
 0. Coins(24) = 6
 0. Coins(25) = 1
 0. Coins(26) = 2
 0. Coins(27) = 3
 0. Coins(28) = 4
 0. Coins(29) = 5
 0. Coins(30) = 2
 0. Coins(31) = 3
 0. Coins(32) = 4
 0. Coins(33) = 5
 0. Coins(34) = 6
 0. Coins(35) = 2
 0. Coins(36) = 3
 0. Coins(37) = 4
 0. Coins(38) = 5
 0. Coins(39) = 6
 0. Coins(40) = 3
 0. Coins(41) = 4
 0. Coins(42) = 5
 0. Coins(43) = 6
 0. Coins(44) = 7
 0. Coins(45) = 3
 0. Coins(46) = 4
 0. Coins(47) = 5
 0. Coins(48) = 6
 0. Coins(49) = 7
 0. Coins(50) = 2
 0. Coins(51) = 3
 0. Coins(52) = 4
 0. Coins(53) = 5
 0. Coins(54) = 6
 0. Coins(55) = 3
 0. Coins(56) = 4
 0. Coins(57) = 5
 0. Coins(58) = 6
 0. Coins(59) = 7
 0. Coins(60) = 3
 0. Coins(61) = 4
 0. Coins(62) = 5
 0. Coins(63) = 6
 0. Coins(64) = 7
 0. Coins(65) = 4
 0. Coins(66) = 5
 0. Coins(67) = 6
 0. Coins(68) = 7
 0. Coins(69) = 8
 0. Coins(70) = 4
 0. Coins(71) = 5
 0. Coins(72) = 6
 0. Coins(73) = 7
 0. Coins(74) = 8
 0. Coins(75) = 3
 0. Coins(76) = 4
 0. Coins(77) = 5
 0. Coins(78) = 6
 0. Coins(79) = 7
 0. Coins(80) = 4
 0. Coins(81) = 5
 0. Coins(82) = 6
 0. Coins(83) = 7
 0. Coins(84) = 8
 0. Coins(85) = 4
 0. Coins(86) = 5
 0. Coins(87) = 6
 0. Coins(88) = 7
 0. Coins(89) = 8
 0. Coins(90) = 5
 0. Coins(91) = 6
 0. Coins(92) = 7
 0. Coins(93) = 8
 0. Coins(94) = 9
 0. Coins(95) = 5
 0. Coins(96) = 6
 0. Coins(97) = 7
 0. Coins(98) = 8
 0. Coins(99) = 9
Hits   = 1412
Misses = 124
EOF
}

run-test() {
    name=$1
    policy=$2
    output=$3
    echo -n "Testing   coins with $name ... "
    valgrind --leak-check=full ./bin/func_coins 6 8 $policy 1 > $SCRATCH/test.log 2> $SCRATCH/valgrind.log
    if [ $? -eq 0 ] && diff -u $SCRATCH/test.log <($output) && [ $(awk '/ERROR SUMMARY:/ {print $4}' $SCRATCH/valgrind.log) -eq 0 ]; then
	echo Success
    else
	echo Failure
	cat $SCRATCH/test.log $SCRATCH/valgrind.log
    fi
}

SCRATCH=$(mktemp -d)
trap "rm -fr $SCRATCH" INT QUIT TERM EXIT

run-test Random 0 coins-random-output
run-test Clock  1 coins-clock-output
