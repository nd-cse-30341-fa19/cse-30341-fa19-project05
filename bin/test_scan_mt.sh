#!/bin/bash

run-test() {
    name=$1
    policy=$2
    echo -n "Testing   scan (mt) with $name ... "
    if ./bin/func_scan 10 64 $policy 8 > /dev/null; then
	echo Success
    else
	echo Failure
    fi
}

SCRATCH=$(mktemp -d)
trap "rm -fr $SCRATCH" INT QUIT TERM EXIT

run-test RANDOM 0
run-test CLOCK  1
