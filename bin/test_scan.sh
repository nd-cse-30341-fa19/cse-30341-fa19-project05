#!/bin/bash

scan-10-output() {
    cat <<EOF
Hits   = 9216
Misses = 1024
EOF
}

scan-8-output() {
    cat <<EOF
Hits   = 0
Misses = 10240
EOF
}

scan-8-random() {
    cat <<EOF
Hits   = 184
Misses = 10056
EOF
}

run-test() {
    name=$1
    policy=$2
    address=$3
    output=$4
    echo -n "Testing   scan ($address) with $name ... "
    if diff -u <(./bin/func_scan $address 64 $policy 1) <($output) > $SCRATCH/test.log; then
	echo Success
    else
	echo Failure
	cat $SCRATCH/test.log
    fi
}

SCRATCH=$(mktemp -d)
trap "rm -fr $SCRATCH" INT QUIT TERM EXIT

run-test Random 0 10 scan-10-output
run-test Clock  1 10 scan-10-output

run-test Random 0 8  scan-8-random
run-test Clock  1 8  scan-8-output
