#!/bin/bash

run-test() {
    name=$1
    policy=$2
    echo -n "Testing   coins (mt) with $name ... "
    valgrind --leak-check=full ./bin/func_coins 8 8 $policy 8 > $SCRATCH/test.log 2> $SCRATCH/valgrind.log
    if [ $? -eq 0 ] && [ $(grep -cEv 'Miss|Hit' $SCRATCH/test.log) = 800 ] && [ $(awk '/ERROR SUMMARY:/ {print $4}' $SCRATCH/valgrind.log) -eq 0 ]; then
	echo Success
    else
	echo Failure
	cat $SCRATCH/test.log $SCRATCH/valgrind.log
    fi
}

SCRATCH=$(mktemp -d)
trap "rm -fr $SCRATCH" INT QUIT TERM EXIT

run-test RANDOM 0
run-test CLOCK  1
