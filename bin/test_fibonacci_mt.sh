#!/bin/bash

run-test() {
    name=$1
    policy=$2
    echo -n "Testing   fibonacci (mt) with $name ... "
    if [ $(./bin/func_fibonacci 6 8 $policy 8 | grep -cEv 'Miss|Hit') = 800 ]; then
	echo Success
    else
	echo Failure
    fi
}

SCRATCH=$(mktemp -d)
trap "rm -fr $SCRATCH" INT QUIT TERM EXIT

run-test RANDOM 0
run-test CLOCK  1
