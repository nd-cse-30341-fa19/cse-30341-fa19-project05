#!/bin/bash

prime-output() {
    cat <<EOF
Hits   = 2048
Misses = 2048
EOF
}

run-test() {
    name=$1
    policy=$2
    output=$3
    echo -n "Testing   prime with $name ... "
    if diff -u <(./bin/func_prime 10 64 $policy 1) <($output) > $SCRATCH/test.log; then
	echo Success
    else
	echo Failure
	cat $SCRATCH/test.log
    fi
}

SCRATCH=$(mktemp -d)
trap "rm -fr $SCRATCH" INT QUIT TERM EXIT

run-test Random 0 prime-output
run-test Clock  1 prime-output
