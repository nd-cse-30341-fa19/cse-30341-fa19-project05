#!/bin/bash

collatz-random-output() {
    cat <<EOF
Collatz(1) = 1
Collatz(2) = 2
Collatz(3) = 8
Collatz(4) = 3
Collatz(5) = 6
Collatz(6) = 9
Collatz(7) = 17
Collatz(8) = 4
Collatz(9) = 20
Collatz(10) = 7
Collatz(11) = 15
Collatz(12) = 10
Collatz(13) = 10
Collatz(14) = 18
Collatz(15) = 18
Collatz(16) = 5
Collatz(17) = 13
Collatz(18) = 21
Collatz(19) = 21
Collatz(20) = 8
Collatz(21) = 8
Collatz(22) = 16
Collatz(23) = 16
Collatz(24) = 11
Collatz(25) = 24
Collatz(26) = 11
Collatz(27) = 112
Collatz(28) = 19
Collatz(29) = 19
Collatz(30) = 19
Collatz(31) = 107
Collatz(32) = 6
Collatz(33) = 27
Collatz(34) = 14
Collatz(35) = 14
Collatz(36) = 22
Collatz(37) = 22
Collatz(38) = 22
Collatz(39) = 35
Collatz(40) = 9
Collatz(41) = 110
Collatz(42) = 9
Collatz(43) = 30
Collatz(44) = 17
Collatz(45) = 17
Collatz(46) = 17
Collatz(47) = 105
Collatz(48) = 12
Collatz(49) = 25
Collatz(50) = 25
Collatz(51) = 25
Collatz(52) = 12
Collatz(53) = 12
Collatz(54) = 113
Collatz(55) = 113
Collatz(56) = 20
Collatz(57) = 33
Collatz(58) = 20
Collatz(59) = 33
Collatz(60) = 20
Collatz(61) = 20
Collatz(62) = 108
Collatz(63) = 108
Collatz(64) = 7
Collatz(65) = 28
Collatz(66) = 28
Collatz(67) = 28
Collatz(68) = 15
Collatz(69) = 15
Collatz(70) = 15
Collatz(71) = 103
Collatz(72) = 23
Collatz(73) = 116
Collatz(74) = 23
Collatz(75) = 15
Collatz(76) = 23
Collatz(77) = 23
Collatz(78) = 36
Collatz(79) = 36
Collatz(80) = 10
Collatz(81) = 23
Collatz(82) = 111
Collatz(83) = 111
Collatz(84) = 10
Collatz(85) = 10
Collatz(86) = 31
Collatz(87) = 31
Collatz(88) = 18
Collatz(89) = 31
Collatz(90) = 18
Collatz(91) = 93
Collatz(92) = 18
Collatz(93) = 18
Collatz(94) = 106
Collatz(95) = 106
Collatz(96) = 13
Collatz(97) = 119
Collatz(98) = 26
Collatz(99) = 26
Collatz(100) = 26
Hits   = 99
Misses = 261
EOF
}

collatz-clock-output() {
    cat <<EOF
Collatz(1) = 1
Collatz(2) = 2
Collatz(3) = 8
Collatz(4) = 3
Collatz(5) = 6
Collatz(6) = 9
Collatz(7) = 17
Collatz(8) = 4
Collatz(9) = 20
Collatz(10) = 7
Collatz(11) = 15
Collatz(12) = 10
Collatz(13) = 10
Collatz(14) = 18
Collatz(15) = 18
Collatz(16) = 5
Collatz(17) = 13
Collatz(18) = 21
Collatz(19) = 21
Collatz(20) = 8
Collatz(21) = 8
Collatz(22) = 16
Collatz(23) = 16
Collatz(24) = 11
Collatz(25) = 24
Collatz(26) = 11
Collatz(27) = 112
Collatz(28) = 19
Collatz(29) = 19
Collatz(30) = 19
Collatz(31) = 107
Collatz(32) = 6
Collatz(33) = 27
Collatz(34) = 14
Collatz(35) = 14
Collatz(36) = 22
Collatz(37) = 22
Collatz(38) = 22
Collatz(39) = 35
Collatz(40) = 9
Collatz(41) = 110
Collatz(42) = 9
Collatz(43) = 30
Collatz(44) = 17
Collatz(45) = 17
Collatz(46) = 17
Collatz(47) = 105
Collatz(48) = 12
Collatz(49) = 25
Collatz(50) = 25
Collatz(51) = 25
Collatz(52) = 12
Collatz(53) = 12
Collatz(54) = 113
Collatz(55) = 113
Collatz(56) = 20
Collatz(57) = 33
Collatz(58) = 20
Collatz(59) = 33
Collatz(60) = 20
Collatz(61) = 20
Collatz(62) = 108
Collatz(63) = 108
Collatz(64) = 7
Collatz(65) = 28
Collatz(66) = 28
Collatz(67) = 28
Collatz(68) = 15
Collatz(69) = 15
Collatz(70) = 15
Collatz(71) = 103
Collatz(72) = 23
Collatz(73) = 116
Collatz(74) = 23
Collatz(75) = 15
Collatz(76) = 23
Collatz(77) = 23
Collatz(78) = 36
Collatz(79) = 36
Collatz(80) = 10
Collatz(81) = 23
Collatz(82) = 111
Collatz(83) = 111
Collatz(84) = 10
Collatz(85) = 10
Collatz(86) = 31
Collatz(87) = 31
Collatz(88) = 18
Collatz(89) = 31
Collatz(90) = 18
Collatz(91) = 93
Collatz(92) = 18
Collatz(93) = 18
Collatz(94) = 106
Collatz(95) = 106
Collatz(96) = 13
Collatz(97) = 119
Collatz(98) = 26
Collatz(99) = 26
Collatz(100) = 26
Hits   = 99
Misses = 265
EOF
}

run-test() {
    name=$1
    policy=$2
    output=$3
    echo -n "Testing   collatz with $name ... "
    seq 1 100 | valgrind --leak-check=full ./bin/func_collatz 8 8 $policy 1 > $SCRATCH/test.log 2> $SCRATCH/valgrind.log
    if [ $? -eq 0 ] && diff -u $SCRATCH/test.log <($output) && [ $(awk '/ERROR SUMMARY:/ {print $4}' $SCRATCH/valgrind.log) -eq 0 ]; then
	echo Success
    else
	echo Failure
	cat $SCRATCH/test.log
    fi
}

SCRATCH=$(mktemp -d)
trap "rm -fr $SCRATCH" INT QUIT TERM EXIT

run-test Random 0 collatz-random-output
run-test Clock  1 collatz-clock-output
