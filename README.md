# Project 05: MemHashed

This is [Project 05] of [CSE.30341.FA19].

## Student

- Domer McDomerson (dmcdomer@nd.edu)

## Brainstorming

### Page

1. For `page_create`, what attributes of the `Page` structure must you
   initialize?  How do you allocate an **array** of `Entry` structures?
   
2. For `page_delete`, what do you need to `free` given the pointer to a `Page`
   structure?
   
3. For `page_get`, how do you perform a **linear probe** starting from a
   particular offset?
   
    - How do you know if you found the right `Entry`?
    
    - What do you need to update when you found an `Entry`?
    
    - What do you return if the `Entry` is not found?

    - Do you need to synchronize access between threads? How?
    
4. For `page_put`, how do you perform a **linear probe** starting from a
   particular offset?
   
    - How do you know if you found an available `Entry`?

    - What should you do if no `Entry` is found?
    
    - What do you need to update when you store the `Entry`?

    - Do you need to synchronize access between threads? How?

### Cache

1. For `cache_create`, what attributes of the `Cache` structure must you
   initialize?  How do you allocate an **array** of `Page` structure pointers?
   
    - How do you compute the `vpn_shift`?
    
    - How do you compute the `vpn_mask`?
    
    - How do you compute the `offset_mask`?

    - How many `npages` do you need to allocate?
    
    - How do you allocate each `Page` in the `Cache`?
   
2. For `cache_delete`, what do you need to `free` given the pointer to a `Cache`
   structure?
   
    - How do you release each `Page` in the `Cache`?
    
3. For `cache_get`, how do you determine which `Page` to lookup?

    - What happens if caching is disabled?

    - How do you compute the `address`?
    
    - How do you compute the `vpn`?
    
    - How do you compute the `offset`?

    - How do you know if you have a **hit** or a **miss**? When do you update
      these counters?

    - Do you need to synchronize access between threads? How?

4. For `cache_put`, how do you determine which `Page` to lookup?

    - How do you compute the `address`?

    - How do you compute the `vpn`?
    
    - How do you compute the `offset`?

## Errata

> Describe any known errors, bugs, or deviations from the requirements.

## Extra Credit

> Describe what extra credit (if any) that you implemented.

[Project 05]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa19/project04.html
[CSE.30341.FA19]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa19/
